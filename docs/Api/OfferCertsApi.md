# Ensi\OffersClient\OfferCertsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOfferCert**](OfferCertsApi.md#createOfferCert) | **POST** /offers/certs | Добавляет новый сертификат оффера
[**deleteOfferCert**](OfferCertsApi.md#deleteOfferCert) | **DELETE** /offers/certs/{id} | Удаление сертификата оффера
[**getOfferCert**](OfferCertsApi.md#getOfferCert) | **GET** /offers/certs/{id} | Получение сертификата оффера по идентификатору
[**patchOfferCert**](OfferCertsApi.md#patchOfferCert) | **PATCH** /offers/certs/{id} | Обновление отдельных атрибутов сертификата оффера
[**searchOfferCerts**](OfferCertsApi.md#searchOfferCerts) | **POST** /offers/certs:search | Поиск сертификаов офферов товаров, удовлетворяющих фильтру
[**uploadOfferCert**](OfferCertsApi.md#uploadOfferCert) | **POST** /offers/certs/{id}:upload-file | Загружает файл сертификата оффера



## createOfferCert

> \Ensi\OffersClient\Dto\OfferCertResponse createOfferCert($create_offer_cert_request)

Добавляет новый сертификат оффера

Добавляет новый сертификат оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OfferCertsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_offer_cert_request = new \Ensi\OffersClient\Dto\CreateOfferCertRequest(); // \Ensi\OffersClient\Dto\CreateOfferCertRequest | 

try {
    $result = $apiInstance->createOfferCert($create_offer_cert_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferCertsApi->createOfferCert: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_offer_cert_request** | [**\Ensi\OffersClient\Dto\CreateOfferCertRequest**](../Model/CreateOfferCertRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\OfferCertResponse**](../Model/OfferCertResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteOfferCert

> \Ensi\OffersClient\Dto\EmptyDataResponse deleteOfferCert($id)

Удаление сертификата оффера

Удаление сертификата оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OfferCertsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteOfferCert($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferCertsApi->deleteOfferCert: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getOfferCert

> \Ensi\OffersClient\Dto\OfferCertResponse getOfferCert($id)

Получение сертификата оффера по идентификатору

Получение сертификата оффера по идентификатору

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OfferCertsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getOfferCert($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferCertsApi->getOfferCert: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\OffersClient\Dto\OfferCertResponse**](../Model/OfferCertResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchOfferCert

> \Ensi\OffersClient\Dto\OfferCertResponse patchOfferCert($id, $patch_offer_cert_request)

Обновление отдельных атрибутов сертификата оффера

Обновление отдельных атрибутов сертификата оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OfferCertsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_offer_cert_request = new \Ensi\OffersClient\Dto\PatchOfferCertRequest(); // \Ensi\OffersClient\Dto\PatchOfferCertRequest | 

try {
    $result = $apiInstance->patchOfferCert($id, $patch_offer_cert_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferCertsApi->patchOfferCert: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_offer_cert_request** | [**\Ensi\OffersClient\Dto\PatchOfferCertRequest**](../Model/PatchOfferCertRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\OfferCertResponse**](../Model/OfferCertResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOfferCerts

> \Ensi\OffersClient\Dto\SearchOfferCertsResponse searchOfferCerts($search_offer_certs_request)

Поиск сертификаов офферов товаров, удовлетворяющих фильтру

Поиск сертификаов офферов, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OfferCertsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_offer_certs_request = new \Ensi\OffersClient\Dto\SearchOfferCertsRequest(); // \Ensi\OffersClient\Dto\SearchOfferCertsRequest | 

try {
    $result = $apiInstance->searchOfferCerts($search_offer_certs_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferCertsApi->searchOfferCerts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_offer_certs_request** | [**\Ensi\OffersClient\Dto\SearchOfferCertsRequest**](../Model/SearchOfferCertsRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\SearchOfferCertsResponse**](../Model/SearchOfferCertsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadOfferCert

> \Ensi\OffersClient\Dto\OfferCertResponse uploadOfferCert($id, $file)

Загружает файл сертификата оффера

Загружает файл сертификата оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OfferCertsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->uploadOfferCert($id, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OfferCertsApi->uploadOfferCert: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\OffersClient\Dto\OfferCertResponse**](../Model/OfferCertResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

