# Ensi\OffersClient\OffersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOffer**](OffersApi.md#createOffer) | **POST** /offers/offers | Запрос на создание нового оффера
[**deleteOffer**](OffersApi.md#deleteOffer) | **DELETE** /offers/offers/{id} | Запрос на удаление оффера
[**deleteOffers**](OffersApi.md#deleteOffers) | **POST** /offers/offers:mass-delete | Массовое удаление офферов
[**getOffer**](OffersApi.md#getOffer) | **GET** /offers/offers/{id} | Запрос оффера по ID
[**patchOffer**](OffersApi.md#patchOffer) | **PATCH** /offers/offers/{id} | Запрос на обновление отдельных полей оффера
[**replaceOffer**](OffersApi.md#replaceOffer) | **PUT** /offers/offers/{id} | Запрос на обновление оффера
[**searchOffers**](OffersApi.md#searchOffers) | **POST** /offers/offers:search | Поиск офферов, удовлетворяющих фильтру



## createOffer

> \Ensi\OffersClient\Dto\OfferResponse createOffer($create_offer_request)

Запрос на создание нового оффера

Запрос на создание нового оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_offer_request = new \Ensi\OffersClient\Dto\CreateOfferRequest(); // \Ensi\OffersClient\Dto\CreateOfferRequest | 

try {
    $result = $apiInstance->createOffer($create_offer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->createOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_offer_request** | [**\Ensi\OffersClient\Dto\CreateOfferRequest**](../Model/CreateOfferRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\OfferResponse**](../Model/OfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteOffer

> \Ensi\OffersClient\Dto\EmptyDataResponse deleteOffer($id)

Запрос на удаление оффера

Запрос на удаление оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteOffer($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->deleteOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteOffers

> \Ensi\OffersClient\Dto\EmptyDataResponse deleteOffers($request_body_mass_delete)

Массовое удаление офферов

Массовое удаление офферов

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$request_body_mass_delete = new \Ensi\OffersClient\Dto\RequestBodyMassDelete(); // \Ensi\OffersClient\Dto\RequestBodyMassDelete | 

try {
    $result = $apiInstance->deleteOffers($request_body_mass_delete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->deleteOffers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body_mass_delete** | [**\Ensi\OffersClient\Dto\RequestBodyMassDelete**](../Model/RequestBodyMassDelete.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getOffer

> \Ensi\OffersClient\Dto\OfferResponse getOffer($id, $include)

Запрос оффера по ID

Запрос оффера по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getOffer($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->getOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OffersClient\Dto\OfferResponse**](../Model/OfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchOffer

> \Ensi\OffersClient\Dto\OfferResponse patchOffer($id, $patch_offer_request)

Запрос на обновление отдельных полей оффера

Запрос на обновление отдельных полей оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_offer_request = new \Ensi\OffersClient\Dto\PatchOfferRequest(); // \Ensi\OffersClient\Dto\PatchOfferRequest | 

try {
    $result = $apiInstance->patchOffer($id, $patch_offer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->patchOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_offer_request** | [**\Ensi\OffersClient\Dto\PatchOfferRequest**](../Model/PatchOfferRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\OfferResponse**](../Model/OfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceOffer

> \Ensi\OffersClient\Dto\OfferResponse replaceOffer($id, $replace_offer_request)

Запрос на обновление оффера

Запрос на обновление оффера

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_offer_request = new \Ensi\OffersClient\Dto\ReplaceOfferRequest(); // \Ensi\OffersClient\Dto\ReplaceOfferRequest | 

try {
    $result = $apiInstance->replaceOffer($id, $replace_offer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->replaceOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_offer_request** | [**\Ensi\OffersClient\Dto\ReplaceOfferRequest**](../Model/ReplaceOfferRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\OfferResponse**](../Model/OfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOffers

> \Ensi\OffersClient\Dto\SearchOffersResponse searchOffers($search_offers_request)

Поиск офферов, удовлетворяющих фильтру

Поиск офферов, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_offers_request = new \Ensi\OffersClient\Dto\SearchOffersRequest(); // \Ensi\OffersClient\Dto\SearchOffersRequest | 

try {
    $result = $apiInstance->searchOffers($search_offers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->searchOffers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_offers_request** | [**\Ensi\OffersClient\Dto\SearchOffersRequest**](../Model/SearchOffersRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\SearchOffersResponse**](../Model/SearchOffersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

