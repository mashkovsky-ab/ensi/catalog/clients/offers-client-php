# Ensi\OffersClient\StocksApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStock**](StocksApi.md#createStock) | **POST** /stocks/stocks | Запрос на создание нового остатка
[**deleteStock**](StocksApi.md#deleteStock) | **DELETE** /stocks/stocks/{id} | Запрос на удаление остатка
[**deleteStocks**](StocksApi.md#deleteStocks) | **POST** /stocks/stocks:mass-delete | Массовое удаление остатков
[**getStock**](StocksApi.md#getStock) | **GET** /stocks/stocks/{id} | Запрос остатка по ID
[**patchStock**](StocksApi.md#patchStock) | **PATCH** /stocks/stocks/{id} | Запрос на обновление отдельных полей остатка
[**replaceStock**](StocksApi.md#replaceStock) | **PUT** /stocks/stocks/{id} | Запрос на обновление остатка
[**reserveStocks**](StocksApi.md#reserveStocks) | **POST** /stocks/stocks:reserve | Резервирование стоков
[**searchStocks**](StocksApi.md#searchStocks) | **POST** /stocks/stocks:search | Поиск остатков, удовлетворяющих фильтру



## createStock

> \Ensi\OffersClient\Dto\StockResponse createStock($create_stock_request)

Запрос на создание нового остатка

Запрос на создание нового остатка

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_stock_request = new \Ensi\OffersClient\Dto\CreateStockRequest(); // \Ensi\OffersClient\Dto\CreateStockRequest | 

try {
    $result = $apiInstance->createStock($create_stock_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->createStock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_stock_request** | [**\Ensi\OffersClient\Dto\CreateStockRequest**](../Model/CreateStockRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\StockResponse**](../Model/StockResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStock

> \Ensi\OffersClient\Dto\EmptyDataResponse deleteStock($id)

Запрос на удаление остатка

Запрос на удаление остатка

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteStock($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->deleteStock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStocks

> \Ensi\OffersClient\Dto\EmptyDataResponse deleteStocks($request_body_mass_delete)

Массовое удаление остатков

Массовое удаление остатков

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$request_body_mass_delete = new \Ensi\OffersClient\Dto\RequestBodyMassDelete(); // \Ensi\OffersClient\Dto\RequestBodyMassDelete | 

try {
    $result = $apiInstance->deleteStocks($request_body_mass_delete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->deleteStocks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body_mass_delete** | [**\Ensi\OffersClient\Dto\RequestBodyMassDelete**](../Model/RequestBodyMassDelete.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStock

> \Ensi\OffersClient\Dto\StockResponse getStock($id, $include)

Запрос остатка по ID

Запрос остатка по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStock($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->getStock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OffersClient\Dto\StockResponse**](../Model/StockResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStock

> \Ensi\OffersClient\Dto\StockResponse patchStock($id, $patch_stock_request)

Запрос на обновление отдельных полей остатка

Запрос на обновление отдельных полей остатка

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_stock_request = new \Ensi\OffersClient\Dto\PatchStockRequest(); // \Ensi\OffersClient\Dto\PatchStockRequest | 

try {
    $result = $apiInstance->patchStock($id, $patch_stock_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->patchStock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_stock_request** | [**\Ensi\OffersClient\Dto\PatchStockRequest**](../Model/PatchStockRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\StockResponse**](../Model/StockResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceStock

> \Ensi\OffersClient\Dto\StockResponse replaceStock($id, $replace_stock_request)

Запрос на обновление остатка

Запрос на обновление остатка

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_stock_request = new \Ensi\OffersClient\Dto\ReplaceStockRequest(); // \Ensi\OffersClient\Dto\ReplaceStockRequest | 

try {
    $result = $apiInstance->replaceStock($id, $replace_stock_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->replaceStock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_stock_request** | [**\Ensi\OffersClient\Dto\ReplaceStockRequest**](../Model/ReplaceStockRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\StockResponse**](../Model/StockResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## reserveStocks

> \Ensi\OffersClient\Dto\EmptyDataResponse reserveStocks($reserve_stocks_request)

Резервирование стоков

Резервирование стоков

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$reserve_stocks_request = new \Ensi\OffersClient\Dto\ReserveStocksRequest(); // \Ensi\OffersClient\Dto\ReserveStocksRequest | 

try {
    $result = $apiInstance->reserveStocks($reserve_stocks_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->reserveStocks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reserve_stocks_request** | [**\Ensi\OffersClient\Dto\ReserveStocksRequest**](../Model/ReserveStocksRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStocks

> \Ensi\OffersClient\Dto\SearchStocksResponse searchStocks($search_stocks_request)

Поиск остатков, удовлетворяющих фильтру

Поиск остатков, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OffersClient\Api\StocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_stocks_request = new \Ensi\OffersClient\Dto\SearchStocksRequest(); // \Ensi\OffersClient\Dto\SearchStocksRequest | 

try {
    $result = $apiInstance->searchStocks($search_stocks_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocksApi->searchStocks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_stocks_request** | [**\Ensi\OffersClient\Dto\SearchStocksRequest**](../Model/SearchStocksRequest.md)|  |

### Return type

[**\Ensi\OffersClient\Dto\SearchStocksResponse**](../Model/SearchStocksResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

