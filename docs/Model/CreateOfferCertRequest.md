# # CreateOfferCertRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название сертификата | [optional] 
**date_end** | [**\DateTime**](\DateTime.md) | Дата окончания действия сертификата | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания сертификата | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления сертификата | [optional] 
**offer_id** | **int** | ID товарного предложения | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


