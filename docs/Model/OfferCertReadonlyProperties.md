# # OfferCertReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор сертификата | [optional] 
**offer_id** | **int** | ID товарного предложения | [optional] 
**file** | [**\Ensi\OffersClient\Dto\File**](File.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


