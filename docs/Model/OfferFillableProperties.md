# # OfferFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **int** | Идентификатор товара, для которого создается оффер | [optional] 
**seller_id** | **int** | Идентификатор продавца | [optional] 
**external_id** | **string** | Код оффера | [optional] 
**storage_address** | **string** | адрес хранения товара в магазине (для сборщика) | [optional] 
**sale_status** | **int** | Статус продажи из OfferSaleStatusEnum | [optional] 
**base_price** | **int** | Базовая цена предложения в коп. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


