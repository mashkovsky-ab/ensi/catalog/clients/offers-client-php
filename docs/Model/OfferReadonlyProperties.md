# # OfferReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор оффера | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания оффера | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления оффера | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


