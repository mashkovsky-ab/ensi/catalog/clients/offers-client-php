# # Stock

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор стока | [optional] 
**store_id** | **int** | ID склада | [optional] 
**offer_id** | **int** | ID товарного предложения | [optional] 
**product_id** | **int** | Идентификатор товара | [optional] 
**qty** | **int** | Количество товара для резервирования | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания остатка | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления остатка | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


