<?php

namespace Ensi\OffersClient;

class OffersClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\OffersClient\Api\StocksApi',
        '\Ensi\OffersClient\Api\OfferCertsApi',
        '\Ensi\OffersClient\Api\OffersApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\OffersClient\Dto\File',
        '\Ensi\OffersClient\Dto\PatchOfferCertRequest',
        '\Ensi\OffersClient\Dto\PatchOfferRequest',
        '\Ensi\OffersClient\Dto\StockFillableProperties',
        '\Ensi\OffersClient\Dto\ModelInterface',
        '\Ensi\OffersClient\Dto\SearchStocksRequest',
        '\Ensi\OffersClient\Dto\OfferCertReadonlyProperties',
        '\Ensi\OffersClient\Dto\Stock',
        '\Ensi\OffersClient\Dto\ErrorResponse',
        '\Ensi\OffersClient\Dto\SearchOffersRequest',
        '\Ensi\OffersClient\Dto\ReplaceOfferRequest',
        '\Ensi\OffersClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\OffersClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\OffersClient\Dto\Error',
        '\Ensi\OffersClient\Dto\OfferFillableProperties',
        '\Ensi\OffersClient\Dto\OfferSaleStatusEnum',
        '\Ensi\OffersClient\Dto\RequestBodyPagination',
        '\Ensi\OffersClient\Dto\OfferIncludes',
        '\Ensi\OffersClient\Dto\PatchStockRequest',
        '\Ensi\OffersClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\OffersClient\Dto\OfferCertResponse',
        '\Ensi\OffersClient\Dto\CreateOfferCertRequestAllOf',
        '\Ensi\OffersClient\Dto\OfferCert',
        '\Ensi\OffersClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\OffersClient\Dto\SearchOfferCertsResponse',
        '\Ensi\OffersClient\Dto\CreateOfferRequest',
        '\Ensi\OffersClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\OffersClient\Dto\RequestBodyCursorPagination',
        '\Ensi\OffersClient\Dto\StockResponse',
        '\Ensi\OffersClient\Dto\MultipartFileUploadRequest',
        '\Ensi\OffersClient\Dto\ReserveStocksRequest',
        '\Ensi\OffersClient\Dto\RequestBodyMassDelete',
        '\Ensi\OffersClient\Dto\SearchStocksResponse',
        '\Ensi\OffersClient\Dto\EmptyDataResponse',
        '\Ensi\OffersClient\Dto\PaginationTypeEnum',
        '\Ensi\OffersClient\Dto\OfferResponse',
        '\Ensi\OffersClient\Dto\CreateStockRequest',
        '\Ensi\OffersClient\Dto\OfferCertFillableProperties',
        '\Ensi\OffersClient\Dto\ResponseBodyPagination',
        '\Ensi\OffersClient\Dto\SearchOffersResponseMeta',
        '\Ensi\OffersClient\Dto\Offer',
        '\Ensi\OffersClient\Dto\CreateOfferCertRequest',
        '\Ensi\OffersClient\Dto\ReserveStocksRequestItems',
        '\Ensi\OffersClient\Dto\ReplaceStockRequest',
        '\Ensi\OffersClient\Dto\SearchOfferCertsRequest',
        '\Ensi\OffersClient\Dto\StockReadonlyProperties',
        '\Ensi\OffersClient\Dto\SearchOffersResponse',
        '\Ensi\OffersClient\Dto\OfferReadonlyProperties',
    ];

    /** @var string */
    public static $configuration = '\Ensi\OffersClient\Configuration';
}
